import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;

/**
 * User: SFedotov
 * Date: 20.02.13
 * Time: 17:50
 */
public class Server extends Thread {

    public static void main(String[] args) throws IOException {
        new Server(1234).start();

        String[][] cmds = {
                {"c:\\work\\alf_fsfr",  "System.out.println"},
                {"c:\\work\\alfresco",  "System.out.println"},
                {"c:\\work\\alfresco2", "System.out.println"}
        };

        for (int i = 0; i < cmds.length; i++) {

            String[] cmd = cmds[i];

            Process exec = Runtime.getRuntime().exec("java -cp target/test_crap-1.0.jar Worker localhost:1234 " + cmd[0] + " " + cmd[1]);
            new Thread(new Gobbler(exec.getErrorStream())).start();
            new Thread(new Gobbler(exec.getInputStream())).start();

        }

    }


    private static ByteBuffer buffer = ByteBuffer.allocate(16 * 1024);
    private final int port;

    public Server(int port) throws IOException {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    private void startServer() throws IOException {
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        ServerSocket socket = serverChannel.socket();
        socket.bind(new InetSocketAddress(port));

        Selector selector = Selector.open();
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            int n = selector.select();

            if (n == 0) {
                continue;
            }

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();

                WritableByteChannel writableByteChannel = Channels.newChannel(System.out);

                if (key.isAcceptable()) {
                    ServerSocketChannel server = (ServerSocketChannel) key.channel();

                    SocketChannel channel = server.accept();
                    if (channel == null) {
                        continue;
                    }

                    channel.configureBlocking(false);
                    channel.register(selector, SelectionKey.OP_READ);
                }

                if (key.isReadable()) {
                    SocketChannel channel = (SocketChannel) key.channel();
                    copyChannels(channel, writableByteChannel);
                }

                iterator.remove();
            }
        }
    }

    private static void copyChannels(ReadableByteChannel src, WritableByteChannel dest) throws IOException {
        buffer.clear();

        while ((src.read(buffer)) != -1) {
            buffer.flip();
            while (buffer.hasRemaining()) {
                dest.write(buffer);
            }
            buffer.clear();
        }

        src.close();

    }

    private static class Gobbler implements Runnable {

        private final InputStream stream;

        private Gobbler(InputStream stream) {
            this.stream = stream;
        }

        @Override
        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(stream);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }

        }
    }

}
