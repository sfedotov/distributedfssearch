import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

/**
 * User: SFedotov
 * Date: 19.02.13
 * Time: 17:07
 */
public class Worker {

    public static void main(String[] args) throws IOException, InterruptedException {

        final String addressToSend = args[0];
        final String path = args[1];
        final String substring = args[2];

        long start = System.currentTimeMillis();

        final SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress("localhost", 1234));

        new MyDirectoryWalker(
                Pattern.compile(Pattern.quote(substring)),
                new File(path),
                new OnFileFoundCallback() {
                    @Override
                    public void handleFile(File file) {
                        String result = file.getAbsolutePath() + "#" + substring;

                        ByteBuffer buffer = ByteBuffer.wrap(result.getBytes());
                        try {
                            socketChannel.write(buffer);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .start();

        System.out.println(System.currentTimeMillis() - start);
    }

    private static interface OnFileFoundCallback {
        void handleFile(File file);
    }

    private static class MyDirectoryWalker {
        private final Pattern pattern;
        private final File startDirectory;
        private final OnFileFoundCallback onFileFoundCallback;

        private MyDirectoryWalker(Pattern pattern, File startDirectory, OnFileFoundCallback onFileFoundCallback) {
            this.pattern = pattern;
            this.startDirectory = startDirectory;
            this.onFileFoundCallback = onFileFoundCallback;
        }

        public void start() throws IOException {
            walk(startDirectory);
        }

        private void walk(File directory) throws IOException {
            File[] childFiles = directory.listFiles();
            if (childFiles == null) {
                throw new NullPointerException("can't list files in: " + directory.getAbsolutePath());
            } else {
                for (File childFile : childFiles) {
                    if (childFile.isDirectory()) {
                        walk(childFile);
                    } else {
                        handleFile(childFile);
                    }
                }
            }
        }

        private void handleFile(File file) throws IOException {
            FileInputStream stream = new FileInputStream(file);
            FileChannel channel = stream.getChannel();
            Charset charset = Charset.forName("UTF-8");

            MappedByteBuffer byteBuffer = channel.map(
                    FileChannel.MapMode.READ_ONLY,
                    0,
                    Math.min(channel.size(), Integer.MAX_VALUE));

            CharBuffer decoded = charset.decode(byteBuffer);

            if (pattern.matcher(decoded).find()) {
                onFileFoundCallback.handleFile(file);
            }
        }
    }
}
